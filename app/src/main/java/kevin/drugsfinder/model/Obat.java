package kevin.drugsfinder.model;

import java.io.Serializable;

/**
 * Created by azisa on 3/4/2017.
 */

public class Obat implements Serializable {
    private int id;
    private String nama_obat;
    private String resep;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getNama_obat() {
        return nama_obat;
    }

    public void setNama_obat(String nama_obat) {
        this.nama_obat = nama_obat;
    }

    public String getResep() {
        return resep;
    }

    public void setResep(String resep) {
        this.resep = resep;
    }
}
