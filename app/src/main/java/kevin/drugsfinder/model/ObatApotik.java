package kevin.drugsfinder.model;

import java.io.Serializable;

/**
 * Created by azisa on 3/4/2017.
 */

public class ObatApotik implements Serializable {
    private int id;
    private int stok;
    private String satuan;
    private long harga;
    private Apotik apotik;
    private Obat obat;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getStok() {
        return stok;
    }

    public void setStok(int stok) {
        this.stok = stok;
    }

    public String getSatuan() {
        return satuan;
    }

    public void setSatuan(String satuan) {
        this.satuan = satuan;
    }

    public long getHarga() {
        return harga;
    }

    public void setHarga(long harga) {
        this.harga = harga;
    }

    public Apotik getApotik() {
        return apotik;
    }

    public void setApotik(Apotik apotik) {
        this.apotik = apotik;
    }

    public Obat getObat() {
        return obat;
    }

    public void setObat(Obat obat) {
        this.obat = obat;
    }
}
