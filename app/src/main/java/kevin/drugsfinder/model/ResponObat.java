package kevin.drugsfinder.model;

import java.io.Serializable;
import java.util.List;

/**
 * Created by azisa on 3/4/2017.
 */

public class ResponObat implements Serializable {
    private int id;
    private String nama_obat;
    private List<ObatApotik> obat_apotik;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getNama_obat() {
        return nama_obat;
    }

    public void setNama_obat(String nama_obat) {
        this.nama_obat = nama_obat;
    }

    public List<ObatApotik> getObat_apotik() {
        return obat_apotik;
    }

    public void setObat_apotik(List<ObatApotik> obat_apotik) {
        this.obat_apotik = obat_apotik;
    }
}
