package kevin.drugsfinder.daftarapotik;

import android.content.Context;
import android.view.View;

import com.tenilodev.aretro.AretroGenerator;

import java.net.ConnectException;
import java.net.SocketTimeoutException;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

import kevin.drugsfinder.Config;
import kevin.drugsfinder.api.ApiService;
import kevin.drugsfinder.model.Apotik;
import kevin.drugsfinder.utils.LocationUtils;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by azisa on 3/11/2017.
 */

public class DaftarApotikPresenter {

    private DaftarApotikView daftarApotikView;

    public DaftarApotikPresenter(DaftarApotikView daftarApotikView) {
        this.daftarApotikView = daftarApotikView;
    }

    public void loadDaftarApotik(final Context context) {
        daftarApotikView.setProgressVisibilitiy(View.VISIBLE);

        ApiService service = AretroGenerator.createService(Config.BASE_URL, ApiService.class);
        service.getAllApotik().enqueue(new Callback<List<Apotik>>() {
            @Override
            public void onResponse(Call<List<Apotik>> call, Response<List<Apotik>> response) {
                daftarApotikView.setProgressVisibilitiy(View.GONE);
                if(response.isSuccessful()){
                    for(int i = 0; i < response.body().size(); i++){
                        response.body().get(i).setJarak(LocationUtils.getInstance().distance(
                                response.body().get(i).getLatitude(),
                                response.body().get(i).getLongitude(),
                                Config.getInstance(context).getCurrentLatLng().latitude,
                                Config.getInstance(context).getCurrentLatLng().longitude
                        ));
                    }
                    daftarApotikView.onResultDaftarApotik(response.body());
                }else{
                    daftarApotikView.onResultError(response.errorBody());
                }
            }

            @Override
            public void onFailure(Call<List<Apotik>> call, Throwable t) {
                daftarApotikView.setProgressVisibilitiy(View.GONE);
                if(t instanceof SocketTimeoutException){
                    daftarApotikView.onRequestTimeout(t);
                }
                if(t instanceof ConnectException){
                    daftarApotikView.onConnectError(t);
                }
            }
        });

    }

    void loadDaftarApotikTerdekat(final Context context) {
        daftarApotikView.setProgressVisibilitiy(View.VISIBLE);

        ApiService service = AretroGenerator.createService(Config.BASE_URL, ApiService.class);
        service.getAllApotik().enqueue(new Callback<List<Apotik>>() {
            @Override
            public void onResponse(Call<List<Apotik>> call, Response<List<Apotik>> response) {
                daftarApotikView.setProgressVisibilitiy(View.GONE);
                if(response.isSuccessful()){
                    for(int i = 0; i < response.body().size(); i++){
                        response.body().get(i).setJarak(LocationUtils.getInstance().distance(
                                response.body().get(i).getLatitude(),
                                response.body().get(i).getLongitude(),
                                Config.getInstance(context).getCurrentLatLng().latitude,
                                Config.getInstance(context).getCurrentLatLng().longitude
                        ));
                    }

                    Collections.sort(response.body(), new Comparator<Apotik>() {
                        @Override
                        public int compare(Apotik o1, Apotik o2) {
                            return Double.compare(o1.getJarak(),o2.getJarak());
                        }
                    });

                    daftarApotikView.onResultDaftarApotik(response.body());
                }else{
                    daftarApotikView.onResultError(response.errorBody());
                }
            }

            @Override
            public void onFailure(Call<List<Apotik>> call, Throwable t) {
                daftarApotikView.setProgressVisibilitiy(View.GONE);
                if(t instanceof SocketTimeoutException){
                    daftarApotikView.onRequestTimeout(t);
                }
                if(t instanceof ConnectException){
                    daftarApotikView.onConnectError(t);
                }
            }
        });
    }
}
