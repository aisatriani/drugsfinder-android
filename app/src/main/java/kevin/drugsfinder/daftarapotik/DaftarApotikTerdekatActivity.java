package kevin.drugsfinder.daftarapotik;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.CoordinatorLayout;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.View;
import android.widget.ProgressBar;

import java.util.List;

import kevin.drugsfinder.R;
import kevin.drugsfinder.adapter.DaftarApotikAdapter;
import kevin.drugsfinder.model.Apotik;
import kevin.drugsfinder.profileapotik.ProfileApotikActivity;
import okhttp3.ResponseBody;

public class DaftarApotikTerdekatActivity extends AppCompatActivity implements DaftarApotikView {

    private ProgressBar progress;
    private RecyclerView rvApotik;
    private CoordinatorLayout rootView;
    private DaftarApotikPresenter presenter;
    private Context context;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_daftar_apotik);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        setTitle("Apotik Terdekat");

        context = this;

        renderView();

        presenter = new DaftarApotikPresenter(this);
        //presenter.loadDaftarApotik(this);
        presenter.loadDaftarApotikTerdekat(this);

    }

    private void renderView() {
        rootView = (CoordinatorLayout)findViewById(R.id.activity_daftar_apotik);
        progress = (ProgressBar) findViewById(R.id.progress);
        rvApotik = (RecyclerView) findViewById(R.id.rv_apotik);
        rvApotik.setLayoutManager(new LinearLayoutManager(this));
    }

    @Override
    public void setProgressVisibilitiy(int visible) {
        progress.setVisibility(visible);
    }

    @Override
    public void onRequestTimeout(Throwable t) {
        presenter.loadDaftarApotik(context);
    }

    @Override
    public void onConnectError(Throwable t) {
        Snackbar.make(rootView, getString(R.string.connect_error), Snackbar.LENGTH_INDEFINITE)
                .setAction("Ulangi", new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        presenter.loadDaftarApotik(context);
                    }
                })
                .show();
    }

    @Override
    public void onResultDaftarApotik(final List<Apotik> body) {
        DaftarApotikAdapter adapter = new DaftarApotikAdapter(this, body);
        rvApotik.setAdapter(adapter);
        adapter.setClickListener(new DaftarApotikAdapter.ClickListener() {
            @Override
            public void onClick(View v, int position) {
                Intent intent = new Intent(context, ProfileApotikActivity.class);
                intent.putExtra("apotik",body.get(position));
                startActivity(intent);
            }
        });
    }

    @Override
    public void onResultError(ResponseBody responseBody) {
        Snackbar.make(rootView, getString(R.string.response_error), Snackbar.LENGTH_INDEFINITE)
                .setAction("Coba lagi", new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        presenter.loadDaftarApotik(context);
                    }
                })
                .show();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
//        getMenuInflater().inflate(R.menu.menu_daftar_apotik, menu);
        return super.onCreateOptionsMenu(menu);
    }
}
