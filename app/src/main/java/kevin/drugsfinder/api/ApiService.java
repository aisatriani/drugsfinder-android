package kevin.drugsfinder.api;

import java.util.List;

import kevin.drugsfinder.model.Apotik;
import kevin.drugsfinder.model.Obat;
import kevin.drugsfinder.model.ObatApotik;
import kevin.drugsfinder.model.ResponObat;
import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Path;

/**
 * Created by azisa on 3/4/2017.
 */

public interface ApiService {

    @GET("api/cariobat/{obat}")
    Call<List<ObatApotik>> cariObat(@Path("obat") String obat);

    @GET("api/apotik")
    Call<List<Apotik>> getAllApotik();

    @GET("api/apotik/{id}/profile")
    Call<List<ObatApotik>> getObatByApotik(@Path("id") int apotik_id);

}
