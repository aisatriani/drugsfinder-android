package kevin.drugsfinder;

import android.content.Context;
import android.content.SharedPreferences;

import com.google.android.gms.maps.model.LatLng;

/**
 * Created by azisa on 2/15/2017.
 */

public class Config {

    public static final String BASE_URL = "http://drugsfinder.herokuapp.com/";
    public static final LatLng DEFAULT_LATLNG = new LatLng(0.556174,123.058548);
    private static final String PREF_NAME = "prefsdata";
    private static final String KEY_LAT = "key.lat";
    private static final String KEY_LON = "key.lon";
    private static Config config;
    private static SharedPreferences pref;

    public static Config getInstance(Context context){
        if(config == null){
            config = new Config();
        }

        pref = context.getSharedPreferences(PREF_NAME, Context.MODE_PRIVATE);

        return config;
    }

    public void setCurrentLatLng(double lat, double lon){
        SharedPreferences.Editor edit = pref.edit();
        edit.putLong(KEY_LAT,Double.doubleToLongBits(lat));
        edit.putLong(KEY_LON,Double.doubleToLongBits(lon));
        edit.apply();
    }

    public LatLng getCurrentLatLng(){
        double lat = Double.longBitsToDouble(pref.getLong(KEY_LAT,0));
        double lon = Double.longBitsToDouble(pref.getLong(KEY_LON,0));
        return new LatLng(lat,lon);
    }


}
