package kevin.drugsfinder.dashboard;

import java.util.List;

import kevin.drugsfinder.model.ObatApotik;

/**
 * Created by azisa on 3/7/2017.
 */

public interface MainActivityView {

    void setProgressBar(int visibility);
    void setVisibilityMainMenu(int visibility);
    void setVisibilitySearchContent(int visibility);
}
