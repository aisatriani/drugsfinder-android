package kevin.drugsfinder.dashboard;

import android.app.SearchManager;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.CoordinatorLayout;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.SearchView;
import android.util.Log;
import android.view.View;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import java.net.ConnectException;
import java.net.SocketTimeoutException;
import java.util.ArrayList;
import java.util.List;

import kevin.drugsfinder.daftarapotik.DaftarApotikActivity;
import kevin.drugsfinder.R;
import kevin.drugsfinder.daftarapotik.DaftarApotikTerdekatActivity;
import kevin.drugsfinder.profileapotik.ProfileApotikActivity;
import kevin.drugsfinder.sebaran.SebaranApotikActivity;
import kevin.drugsfinder.adapter.ObatApotikAdapter;
import kevin.drugsfinder.model.ObatApotik;
import okhttp3.ResponseBody;

public class MainActivity extends AppCompatActivity
        implements NavigationView.OnNavigationItemSelectedListener, MainActivityRepository, MainActivityView, View.OnClickListener {

    private SearchView searchView;
    private SearchView.OnQueryTextListener queryTextListener;
    private LinearLayout layoutSearchResult;
    private TextView textNoResult;
    private RecyclerView rvSearchResult;
    private Context context;
    private ProgressBar progress;
    private MainActivityPresenter presenter;
    private CoordinatorLayout mainCoordinatorLayout;
    private ImageView actionMenuDaftarapotik, actionMenuSebaranApotik, actionMenuApotikTerdekat, actionMenuAbout;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setTitle("cari obat");

        context = this;

        toolbar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                searchView.setFocusable(true);
                searchView.requestFocus();
                searchView.setIconified(false);
                searchView.requestFocusFromTouch();
            }
        });

        renderMenuDashboard();

        mainCoordinatorLayout = (CoordinatorLayout) findViewById(R.id.main_coordinator_layout);
        layoutSearchResult = (LinearLayout) findViewById(R.id.layout_search_result);
        textNoResult = (TextView) findViewById(R.id.text_no_result);
        rvSearchResult = (RecyclerView) findViewById(R.id.rv_search_result);

        rvSearchResult.setLayoutManager(new LinearLayoutManager(this));
        progress = (ProgressBar)findViewById(R.id.progress);
        progress.setVisibility(View.GONE);

        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Snackbar.make(view, "Replace with your own action", Snackbar.LENGTH_LONG)
                        .setAction("Action", null).show();
            }
        });

        fab.setVisibility(View.GONE);

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.setDrawerListener(toggle);
        toggle.syncState();
        toggle.setDrawerIndicatorEnabled(false);

        NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);

        presenter = new MainActivityPresenter(this, this);

    }

    private void renderMenuDashboard() {
        actionMenuDaftarapotik = (ImageView) findViewById(R.id.action_menu_daftarapotik);
        actionMenuSebaranApotik = (ImageView) findViewById(R.id.action_menu_sebaran_apotik);
        actionMenuApotikTerdekat = (ImageView) findViewById(R.id.action_menu_apotik_terdekat);
        actionMenuAbout = (ImageView) findViewById(R.id.action_menu_about);

        actionMenuDaftarapotik.setOnClickListener(this);
        actionMenuSebaranApotik.setOnClickListener(this);
        actionMenuAbout.setOnClickListener(this);
        actionMenuApotikTerdekat.setOnClickListener(this);
    }

    @Override
    public void onBackPressed() {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {

            AlertDialog.Builder builder = new AlertDialog.Builder(this);
            builder.setTitle("Konfirmasi Keluar");
            builder.setMessage("Yakin ingin keluar dari aplikasi?");
            builder.setPositiveButton("YA", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    MainActivity.super.onBackPressed();
                }
            });

            builder.setNegativeButton("TIDAK", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {

                }
            });

            builder.create().show();



        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.main, menu);

        MenuItem searchItem = menu.findItem(R.id.action_search);

        SearchManager searchManager = (SearchManager) getSystemService(Context.SEARCH_SERVICE);

        if (searchItem != null) {
            searchView = (SearchView) searchItem.getActionView();
        }
        if (searchView != null) {
            searchView.setSearchableInfo(searchManager.getSearchableInfo(getComponentName()));

            queryTextListener = new SearchView.OnQueryTextListener() {
                @Override
                public boolean onQueryTextChange(String newText) {
                    Log.i("onQueryTextChange", newText);
                    if(newText.length() > 0)
                        presenter.cariObat(context, newText);
                        //cariObat(newText);

                    else{
                        presenter.batalCariObat();
                        ObatApotikAdapter adapter = new ObatApotikAdapter(context, new ArrayList<ObatApotik>());
                        rvSearchResult.setAdapter(adapter);
                    }

                    return true;
                }
                @Override
                public boolean onQueryTextSubmit(String query) {
                    Log.i("onQueryTextSubmit", query);

                    return true;
                }
            };
            searchView.setOnQueryTextListener(queryTextListener);

        }

        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_search) {
            return false;
        }

        searchView.setOnQueryTextListener(queryTextListener);

        return super.onOptionsItemSelected(item);
    }

    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        // Handle navigation view item clicks here.
        int id = item.getItemId();

        if (id == R.id.nav_camera) {
            // Handle the camera action
        } else if (id == R.id.nav_gallery) {

        } else if (id == R.id.nav_slideshow) {

        } else if (id == R.id.nav_manage) {

        } else if (id == R.id.nav_share) {

        } else if (id == R.id.nav_send) {

        }

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }

    @Override
    public void hasilPencarianObat(final List<ObatApotik> obatApotikList) {
        ObatApotikAdapter adapter = new ObatApotikAdapter(this, obatApotikList);
        rvSearchResult.setAdapter(adapter);
        adapter.setClickListener(new ObatApotikAdapter.ClickListener() {
            @Override
            public void onClick(View v, int position) {
                Intent intent = new Intent(context, ProfileApotikActivity.class);
                intent.putExtra("apotik", obatApotikList.get(position).getApotik());
                startActivity(intent);
            }
        });
    }

    @Override
    public void connectionError(Throwable t) {
        t.printStackTrace();
        System.out.println(t.getMessage());
        if(t instanceof SocketTimeoutException){
            Toast.makeText(context, "connect timeout", Toast.LENGTH_SHORT).show();
        }
        if(t instanceof ConnectException){
            Toast.makeText(context, "gagal terhubung ke server", Toast.LENGTH_SHORT).show();
        }
    }

    @Override
    public void resposeError(ResponseBody responseBody) {
        Toast.makeText(context, "response error", Toast.LENGTH_SHORT).show();
    }

    @Override
    public void setProgressBar(int visibility) {
        progress.setVisibility(visibility);
    }

    @Override
    public void setVisibilityMainMenu(int visibility) {
        findViewById(R.id.layout_menu_dashboard).setVisibility(visibility);
    }

    @Override
    public void setVisibilitySearchContent(int visibility) {
        findViewById(R.id.layout_search_result).setVisibility(visibility);
        if(visibility == View.VISIBLE)
            mainCoordinatorLayout.setBackgroundColor(getResources().getColor(R.color.md_grey_300));
        if(visibility == View.GONE)
            mainCoordinatorLayout.setBackgroundResource(R.drawable.bg_hp);
    }

    @Override
    public void onClick(View v) {
        if(v.getId() == R.id.action_menu_daftarapotik){
            Intent intent = new Intent(this, DaftarApotikActivity.class);
            startActivity(intent);
        }
        if(v.getId() == R.id.action_menu_sebaran_apotik){
            Intent intent = new Intent(this, SebaranApotikActivity.class);
            startActivity(intent);
        }
        if(v.getId() == R.id.action_menu_apotik_terdekat){
            Intent intent = new Intent(this, DaftarApotikTerdekatActivity.class);
            startActivity(intent);
        }
        if(v.getId() == R.id.action_menu_about){
            actionAbout();
        }
    }

    private void actionAbout() {
        new AlertDialog.Builder(this)
                .setTitle("Tentang Aplikasi")
                .setMessage("Aplikasi ini di buat untuk memenuhi skripsi akhir saya.")
                .create().show();
    }
}
