package kevin.drugsfinder.dashboard;

import java.util.List;

import kevin.drugsfinder.model.ObatApotik;
import okhttp3.ResponseBody;

/**
 * Created by azisa on 3/7/2017.
 */

public interface MainActivityRepository {
    void hasilPencarianObat(List<ObatApotik> obatApotikList);
    void connectionError(Throwable t);
    void resposeError(ResponseBody responseBody);
}
