package kevin.drugsfinder.dashboard;

import android.content.Context;
import android.view.View;

import com.tenilodev.aretro.AretroGenerator;

import java.util.List;

import kevin.drugsfinder.Config;
import kevin.drugsfinder.api.ApiService;
import kevin.drugsfinder.model.ObatApotik;
import kevin.drugsfinder.utils.LocationUtils;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by azisa on 3/7/2017.
 */

public class MainActivityPresenter {

    private final MainActivityView view;
    private final MainActivityRepository repository;

    public MainActivityPresenter(MainActivityView view, MainActivityRepository repository) {
        this.view = view;
        this.repository = repository;
    }

    public void batalCariObat(){
        view.setProgressBar(View.GONE);
        view.setVisibilityMainMenu(View.VISIBLE);
        view.setVisibilitySearchContent(View.GONE);
    }

    public void cariObat(final Context context, String query){
        view.setProgressBar(View.VISIBLE);
        view.setVisibilityMainMenu(View.GONE);
        view.setVisibilitySearchContent(View.VISIBLE);
        ApiService service = AretroGenerator.createService(Config.BASE_URL, ApiService.class);
        Call<List<ObatApotik>> call = service.cariObat(query);
        call.enqueue(new Callback<List<ObatApotik>>() {
            @Override
            public void onResponse(Call<List<ObatApotik>> call, Response<List<ObatApotik>> response) {
                view.setProgressBar(View.GONE);
                if(response.isSuccessful()){
                    for(int i = 0; i < response.body().size() ; i++){
                        response.body().get(i).getApotik().setJarak(LocationUtils.getInstance().distance(
                                response.body().get(i).getApotik().getLatitude(),
                                response.body().get(i).getApotik().getLongitude(),
                                Config.getInstance(context).getCurrentLatLng().latitude,
                                Config.getInstance(context).getCurrentLatLng().longitude
                        ));
                    }
                    repository.hasilPencarianObat(response.body());
                }else {
                    repository.resposeError(response.errorBody());
                }

            }

            @Override
            public void onFailure(Call<List<ObatApotik>> call, Throwable t) {
                view.setProgressBar(View.GONE);
                repository.connectionError(t);
            }
        });
    }
}
