package kevin.drugsfinder.sebaran;

import java.util.List;

import kevin.drugsfinder.model.Apotik;
import okhttp3.ResponseBody;

/**
 * Created by azisa on 3/11/2017.
 */

public interface SebaranApotikView {
    void setProgressVisibilitiy(int visible);
    void onRequestTimeout(Throwable t);
    void onConnectError(Throwable t);
    void onResultDaftarApotik(List<Apotik> body);
    void onResultError(ResponseBody responseBody);
}
