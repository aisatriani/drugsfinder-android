package kevin.drugsfinder.sebaran;

import android.view.View;

import com.tenilodev.aretro.AretroGenerator;

import java.net.ConnectException;
import java.net.SocketTimeoutException;
import java.util.List;

import kevin.drugsfinder.Config;
import kevin.drugsfinder.api.ApiService;
import kevin.drugsfinder.model.Apotik;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by azisa on 3/11/2017.
 */

public class SebaranApotikPresenter {

    private SebaranApotikView sebaranApotikView;

    public SebaranApotikPresenter(SebaranApotikView sebaranApotikView) {
        this.sebaranApotikView = sebaranApotikView;
    }

    public void loadAllApotik() {
        sebaranApotikView.setProgressVisibilitiy(View.VISIBLE);

        ApiService service = AretroGenerator.createService(Config.BASE_URL, ApiService.class);
        service.getAllApotik().enqueue(new Callback<List<Apotik>>() {
            @Override
            public void onResponse(Call<List<Apotik>> call, Response<List<Apotik>> response) {
                sebaranApotikView.setProgressVisibilitiy(View.GONE);
                if(response.isSuccessful()){
                    sebaranApotikView.onResultDaftarApotik(response.body());
                }else{
                    sebaranApotikView.onResultError(response.errorBody());
                }
            }

            @Override
            public void onFailure(Call<List<Apotik>> call, Throwable t) {
                sebaranApotikView.setProgressVisibilitiy(View.GONE);
                if(t instanceof SocketTimeoutException){
                    sebaranApotikView.onRequestTimeout(t);
                }
                if(t instanceof ConnectException){
                    sebaranApotikView.onConnectError(t);
                }
            }
        });

    }
}
