package kevin.drugsfinder.sebaran;

import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.support.design.widget.Snackbar;
import android.support.v4.app.FragmentActivity;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.view.View;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import kevin.drugsfinder.Config;
import kevin.drugsfinder.R;
import kevin.drugsfinder.model.Apotik;
import kevin.drugsfinder.profileapotik.ProfileApotikActivity;
import okhttp3.ResponseBody;

public class SebaranApotikActivity extends FragmentActivity implements OnMapReadyCallback, SebaranApotikView, GoogleMap.OnMarkerClickListener {

    private GoogleMap mMap;
    private SebaranApotikPresenter presenter;
    private Map<Marker, Apotik> markersApotik = new HashMap<>();
    private Context context;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sebaran_apotik);
        // Obtain the SupportMapFragment and get notified when the map is ready to be used.
        context = this;
        presenter = new SebaranApotikPresenter(this);

        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager()
                .findFragmentById(R.id.map);
        mapFragment.getMapAsync(this);
    }


    @Override
    public void onMapReady(GoogleMap googleMap) {
        mMap = googleMap;
        // Add a marker in Sydney and move the camera
        LatLng gorontalo = Config.DEFAULT_LATLNG;
        mMap.moveCamera(CameraUpdateFactory.newLatLngZoom(gorontalo,13));
        presenter.loadAllApotik();
    }

    @Override
    public void setProgressVisibilitiy(int visible) {

    }

    @Override
    public void onRequestTimeout(Throwable t) {
        presenter.loadAllApotik();
    }

    @Override
    public void onConnectError(Throwable t) {
        Snackbar.make(findViewById(R.id.map), getString(R.string.connect_error), Snackbar.LENGTH_INDEFINITE)
                .setAction("Ulangi", new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        presenter.loadAllApotik();
                    }
                })
                .show();
    }

    @Override
    public void onResultDaftarApotik(List<Apotik> body) {
        for(Apotik apotik : body){
            Marker marker = mMap.addMarker(new MarkerOptions()
                    .title(apotik.getNama())
                    .position(new LatLng(apotik.getLatitude(), apotik.getLongitude()))
                    .snippet(apotik.getNama())
            );
            markersApotik.put(marker, apotik);
        }

        mMap.setOnMarkerClickListener(this);
    }

    @Override
    public void onResultError(ResponseBody responseBody) {
        Snackbar.make(findViewById(R.id.map), getString(R.string.response_error), Snackbar.LENGTH_INDEFINITE)
                .setAction("Ulangi", new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        presenter.loadAllApotik();
                    }
                })
                .show();
    }

    @Override
    public boolean onMarkerClick(Marker marker) {
        final Apotik apotik = markersApotik.get(marker);

        new AlertDialog.Builder(this)
                .setCancelable(false)
                .setTitle(apotik.getNama())
                .setMessage(String.format("%s", apotik.getAlamat()))
                .setPositiveButton("Buka Profile", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        Intent intent = new Intent(context, ProfileApotikActivity.class);
                        intent.putExtra("apotik",apotik);
                        startActivity(intent);
                    }
                })
                .setNegativeButton("Batal", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {

                    }
                })
                .create().show();

        return true;
    }
}
