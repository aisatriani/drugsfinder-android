package kevin.drugsfinder.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.math.RoundingMode;
import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.util.List;

import kevin.drugsfinder.R;
import kevin.drugsfinder.model.Apotik;

public class DaftarApotikAdapter extends RecyclerView.Adapter<DaftarApotikAdapter.ViewHolder> {

    private List<Apotik> list;
    private ClickListener clickListener;
    private Context context;

    // Provide a reference to the views for each data item
    // Complex data items may need more than one view per item, and
    // you provide access to all the views for a data item in a view holder
    static class ViewHolder extends RecyclerView.ViewHolder {
        private final TextView title;
        private final TextView alamat;
        private final TextView jarak;
        // each data item is just a string in this case

        //TextView tv_title;
        View view;


        ViewHolder(View v) {

            super(v);
            this.view = v;

            title = (TextView) view.findViewById(R.id.title);
            alamat = (TextView) view.findViewById(R.id.alamat);
            jarak = (TextView) view.findViewById(R.id.jarak);


        }
    }

    // Provide a suitable constructor (depends on the kind of dataset)
    public DaftarApotikAdapter(Context context, List<Apotik> myDataset) {
        this.context = context;
        list = myDataset;
    }

    // Create new views (invoked by the layout manager)
    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        // create a new view
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_daftar_apotik, parent, false);
        ViewHolder vh = new ViewHolder(v);
        return vh;
    }

    // Replace the contents of a view (invoked by the layout manager)
    @Override
    public void onBindViewHolder(final ViewHolder holder, final int position) {
        Apotik apotik = list.get(position);
        holder.title.setText(apotik.getNama());
        holder.alamat.setText(apotik.getAlamat());
        double jarak = apotik.getJarak();
        NumberFormat instance = DecimalFormat.getInstance();
        instance.setRoundingMode(RoundingMode.UP);
        String format = instance.format(jarak);
        holder.jarak.setText(format + " Km");

        holder.view.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (clickListener != null)
                    clickListener.onClick(view, position);
            }
        });


    }

    // Return the size of your dataset (invoked by the layout manager)
    @Override
    public int getItemCount() {
        return list.size();
    }

    public interface ClickListener {
        void onClick(View v, int position);
    }

    public void setClickListener(ClickListener listener) {
        this.clickListener = listener;
    }

}