package kevin.drugsfinder.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.text.NumberFormat;
import java.util.Currency;
import java.util.List;
import java.util.Locale;

import kevin.drugsfinder.R;
import kevin.drugsfinder.model.ObatApotik;

public class ProfileObatApotikAdapter extends RecyclerView.Adapter<ProfileObatApotikAdapter.ViewHolder> {

    private List<ObatApotik> list;
    private ClickListener clickListener;
    private Context context;

    // Provide a reference to the views for each data item
    // Complex data items may need more than one view per item, and
    // you provide access to all the views for a data item in a view holder
    static class ViewHolder extends RecyclerView.ViewHolder {

        private final TextView textItemObat;
        private final TextView textItemSatuan;
        private final TextView textItemHarga;
        private final TextView textStatusStok;
        private final TextView textItemStok;
        private final TextView textItemResep;

        View view;


        ViewHolder(View v) {

            super(v);
            this.view = v;


            textItemObat = (TextView) view.findViewById(R.id.text_item_obat);
            textItemSatuan = (TextView) view.findViewById(R.id.text_item_satuan);
            textItemHarga = (TextView) view.findViewById(R.id.text_item_harga);
            textStatusStok = (TextView) view.findViewById(R.id.status_stok);
            textItemStok = (TextView)view.findViewById(R.id.text_item_stok);
            textItemResep = (TextView) view.findViewById(R.id.text_item_resep);


        }
    }

    // Provide a suitable constructor (depends on the kind of dataset)
    public ProfileObatApotikAdapter(Context context, List<ObatApotik> myDataset) {
        this.context = context;
        list = myDataset;
    }

    // Create new views (invoked by the layout manager)
    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        // create a new view
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_profile_obat_apotik, parent, false);
        ViewHolder vh = new ViewHolder(v);
        return vh;
    }

    // Replace the contents of a view (invoked by the layout manager)
    @Override
    public void onBindViewHolder(final ViewHolder holder, final int position) {
        //holder.tv_title.setText(list.get(position).getNama_tour());
        ObatApotik obatApotik = list.get(position);
        holder.textItemObat.setText(obatApotik.getObat().getNama_obat());
        holder.textItemResep.setText(obatApotik.getObat().getResep());
        holder.textItemSatuan.setText(obatApotik.getSatuan().toLowerCase());

        holder.textItemStok.setText(String.valueOf(obatApotik.getStok()));

        Currency currency = Currency.getInstance(Locale.UK);
        NumberFormat numberFormat = NumberFormat.getInstance();
        numberFormat.setCurrency(currency);
        String format = numberFormat.format(obatApotik.getHarga());
        holder.textItemHarga.setText("Rp "+format);


        if(obatApotik.getStok() != 0){
            holder.textStatusStok.setText("tersedia");
            holder.textStatusStok.setBackgroundColor(context.getResources().getColor(R.color.md_green_500));
        }else{
            holder.textStatusStok.setText("kosong");
            holder.textStatusStok.setBackgroundColor(context.getResources().getColor(R.color.md_red_500));
        }

        holder.view.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (clickListener != null)
                    clickListener.onClick(view, position);
            }
        });


    }

    // Return the size of your dataset (invoked by the layout manager)
    @Override
    public int getItemCount() {
        return list.size();
    }

    public interface ClickListener {
        void onClick(View v, int position);
    }

    public void setClickListener(ClickListener listener) {
        this.clickListener = listener;
    }

}