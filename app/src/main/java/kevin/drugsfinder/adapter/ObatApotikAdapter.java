package kevin.drugsfinder.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.math.RoundingMode;
import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.util.List;
import java.util.Locale;

import kevin.drugsfinder.R;
import kevin.drugsfinder.model.ObatApotik;

public class ObatApotikAdapter extends RecyclerView.Adapter<ObatApotikAdapter.ViewHolder> {

    private List<ObatApotik> list;
    private ClickListener clickListener;
    private Context context;

    // Provide a reference to the views for each data item
    // Complex data items may need more than one view per item, and
    // you provide access to all the views for a data item in a view holder
    static class ViewHolder extends RecyclerView.ViewHolder {

        private final TextView textItemObat;
        private final TextView textItemApotik;
        private final TextView textItemSatuan;
        private final TextView textItemJarak;
        private final TextView textItemHarga;
        private final TextView textItemStok;
        private final TextView textItemResep;


        View view;


        ViewHolder(View v) {

            super(v);
            this.view = v;


            textItemObat = (TextView) view.findViewById(R.id.text_item_obat);
            textItemApotik = (TextView) view.findViewById(R.id.text_item_apotik);
            textItemSatuan = (TextView) view.findViewById(R.id.text_item_satuan);
            textItemJarak = (TextView) view.findViewById(R.id.text_item_jarak);
            textItemHarga = (TextView) view.findViewById(R.id.text_item_harga);
            textItemStok = (TextView)view.findViewById(R.id.text_item_stok);
            textItemResep = (TextView) view.findViewById(R.id.text_item_resep);


        }
    }

    // Provide a suitable constructor (depends on the kind of dataset)
    public ObatApotikAdapter(Context context, List<ObatApotik> myDataset) {
        this.context = context;
        list = myDataset;
    }

    // Create new views (invoked by the layout manager)
    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        // create a new view
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_obat_apotik, parent, false);
        ViewHolder vh = new ViewHolder(v);
        return vh;
    }

    // Replace the contents of a view (invoked by the layout manager)
    @Override
    public void onBindViewHolder(final ViewHolder holder, final int position) {

        NumberFormat instance = DecimalFormat.getInstance();

        //holder.tv_title.setText(list.get(position).getNama_tour());
        ObatApotik obatApotik = list.get(position);
        holder.textItemObat.setText(obatApotik.getObat().getNama_obat());
        holder.textItemApotik.setText(obatApotik.getApotik().getNama());
        holder.textItemResep.setText(obatApotik.getObat().getResep());
        holder.textItemStok.setText("stok : "+ String.valueOf(obatApotik.getStok()));

        String harga = instance.format(obatApotik.getHarga());

        holder.textItemHarga.setText("Rp "+harga);
        holder.textItemSatuan.setText(obatApotik.getSatuan());

        double jarak = obatApotik.getApotik().getJarak();

        instance.setRoundingMode(RoundingMode.UP);
        String format = instance.format(jarak);
        holder.textItemJarak.setText(format + " Km");

        holder.view.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (clickListener != null)
                    clickListener.onClick(view, position);
            }
        });


    }

    // Return the size of your dataset (invoked by the layout manager)
    @Override
    public int getItemCount() {
        return list.size();
    }

    public interface ClickListener {
        void onClick(View v, int position);
    }

    public void setClickListener(ClickListener listener) {
        this.clickListener = listener;
    }

}