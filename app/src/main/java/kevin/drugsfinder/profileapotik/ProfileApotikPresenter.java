package kevin.drugsfinder.profileapotik;

import android.view.View;

import com.tenilodev.aretro.AretroGenerator;

import java.net.ConnectException;
import java.net.SocketTimeoutException;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

import kevin.drugsfinder.Config;
import kevin.drugsfinder.api.ApiService;
import kevin.drugsfinder.model.ObatApotik;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by azisa on 3/13/2017.
 */

public class ProfileApotikPresenter {

    public static final int SORT_ALFA = 1;
    public static final int SORT_TERMURAH = 2;
    public static final int SORT_TERMAHAL = 3;

    private ProfileApotikView profileApotikView;

    public ProfileApotikPresenter(ProfileApotikView profileApotikView) {
        this.profileApotikView = profileApotikView;
    }

    public void loadDataObat(int id) {
        profileApotikView.onProgressVisibility(View.VISIBLE);

        ApiService service = AretroGenerator.createService(Config.BASE_URL, ApiService.class);
        Call<List<ObatApotik>> call = service.getObatByApotik(id);
        call.enqueue(new Callback<List<ObatApotik>>() {
            @Override
            public void onResponse(Call<List<ObatApotik>> call, Response<List<ObatApotik>> response) {
                profileApotikView.onProgressVisibility(View.GONE);
                if(response.isSuccessful()){
                    profileApotikView.onResponseSuccess(response.body());
                }else{
                    profileApotikView.onResponseError(response.errorBody());
                }
            }

            @Override
            public void onFailure(Call<List<ObatApotik>> call, Throwable t) {
                profileApotikView.onProgressVisibility(View.GONE);
                if(t instanceof SocketTimeoutException){
                    profileApotikView.onRequestTimeout(t);
                }
                if(t instanceof ConnectException){
                    profileApotikView.onConnectError(t);
                }
            }
        });
    }

    public void sortData(List<ObatApotik> obatApotiks, int sort) {
        if(sort == SORT_ALFA){
            Collections.sort(obatApotiks, new Comparator<ObatApotik>() {
                @Override
                public int compare(ObatApotik o1, ObatApotik o2) {
                    return o1.getObat().getNama_obat().compareTo(o2.getObat().getNama_obat());
                }
            });
            profileApotikView.onSortData(obatApotiks);
        }
        if(sort == SORT_TERMURAH){
            Collections.sort(obatApotiks, new Comparator<ObatApotik>() {
                @Override
                public int compare(ObatApotik o1, ObatApotik o2) {
                    return Long.valueOf(o1.getHarga()).compareTo(Long.valueOf(o2.getHarga()));
                }
            });
            profileApotikView.onSortData(obatApotiks);
        }

        if(sort == SORT_TERMAHAL){
            Collections.sort(obatApotiks, new Comparator<ObatApotik>() {
                @Override
                public int compare(ObatApotik o1, ObatApotik o2) {
                    return Long.valueOf(o2.getHarga()).compareTo(Long.valueOf(o1.getHarga()));
                }
            });
            profileApotikView.onSortData(obatApotiks);
        }
    }
}
