package kevin.drugsfinder.profileapotik;

import java.util.List;

import kevin.drugsfinder.model.ObatApotik;
import okhttp3.ResponseBody;

/**
 * Created by azisa on 3/13/2017.
 */

public interface ProfileApotikView {
    void onProgressVisibility(int visible);

    void onResponseError(ResponseBody responseBody);

    void onResponseSuccess(List<ObatApotik> body);

    void onRequestTimeout(Throwable t);

    void onConnectError(Throwable t);

    void onSortData(List<ObatApotik> obatApotiks);
}
