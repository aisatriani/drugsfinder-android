package kevin.drugsfinder.profileapotik;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;

import java.util.List;
import java.util.Locale;

import kevin.drugsfinder.Config;
import kevin.drugsfinder.R;
import kevin.drugsfinder.adapter.ProfileObatApotikAdapter;
import kevin.drugsfinder.model.Apotik;
import kevin.drugsfinder.model.ObatApotik;
import okhttp3.ResponseBody;

public class ProfileApotikActivity extends AppCompatActivity implements OnMapReadyCallback, ProfileApotikView {


    private GoogleMap mMap;
    private Apotik mApotik;
    private TextView textAlamat;
    private ProfileApotikPresenter presenter;
    private RecyclerView rvObat;
    private ProgressBar progress;
    private List<ObatApotik> obatApotiks;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_profile_apotik);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        presenter = new ProfileApotikPresenter(this);

        renderView();
        handleIntent(getIntent());


        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String uri = String.format(Locale.ENGLISH, "google.navigation:q=%f,%f", mApotik.getLatitude(), mApotik.getLongitude());
                Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse(uri));
                startActivity(intent);
            }
        });

        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager()
                .findFragmentById(R.id.map);
        mapFragment.getMapAsync(this);


    }

    private void renderView() {
        textAlamat = (TextView)findViewById(R.id.alamat);
        progress = (ProgressBar)findViewById(R.id.progress);
        progress.setVisibility(View.GONE);
        rvObat = (RecyclerView)findViewById(R.id.rv_profile_obat_apotik);
        rvObat.setLayoutManager(new LinearLayoutManager(this));
    }

    private void handleIntent(Intent intent) {
        mApotik = (Apotik)intent.getSerializableExtra("apotik");
        setTitle(mApotik.getNama());
        textAlamat.setText(mApotik.getAlamat());

        presenter.loadDataObat(mApotik.getId());
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        mMap = googleMap;
        LatLng target = new LatLng(mApotik.getLatitude(), mApotik.getLongitude());
        mMap.addMarker(new MarkerOptions().title(
                mApotik.getNama())
                .position(target)
        );
        mMap.moveCamera(CameraUpdateFactory.newLatLngZoom(target,16));
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_profile_apotik,menu);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if(item.getItemId() == R.id.action_sort_alfa){
            presenter.sortData(obatApotiks, ProfileApotikPresenter.SORT_ALFA);
        }
        if(item.getItemId() == R.id.action_sort_termurah){
            presenter.sortData(obatApotiks, ProfileApotikPresenter.SORT_TERMURAH);
        }
        if(item.getItemId() == R.id.action_sort_termahal){
            presenter.sortData(obatApotiks, ProfileApotikPresenter.SORT_TERMAHAL);
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onProgressVisibility(int visible) {
        progress.setVisibility(visible);
    }

    @Override
    public void onResponseError(ResponseBody responseBody) {
        Snackbar.make(findViewById(R.id.container), getString(R.string.response_error), Snackbar.LENGTH_INDEFINITE)
                .setAction("Ulangi", new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        presenter.loadDataObat(mApotik.getId());
                    }
                })
                .show();
    }

    @Override
    public void onResponseSuccess(List<ObatApotik> body) {
        obatApotiks = body;
        ProfileObatApotikAdapter adapter = new ProfileObatApotikAdapter(this,body);
        rvObat.setAdapter(adapter);
    }

    @Override
    public void onRequestTimeout(Throwable t) {
        presenter.loadDataObat(mApotik.getId());
    }

    @Override
    public void onConnectError(Throwable t) {
        Snackbar.make(findViewById(R.id.container), getString(R.string.connect_error), Snackbar.LENGTH_INDEFINITE)
                .setAction("Ulangi", new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        presenter.loadDataObat(mApotik.getId());
                    }
                })
                .show();
    }

    @Override
    public void onSortData(List<ObatApotik> obatApotiks) {
        ProfileObatApotikAdapter adapter = new ProfileObatApotikAdapter(this,obatApotiks);
        rvObat.setAdapter(adapter);
    }
}
