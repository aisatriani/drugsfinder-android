package kevin.drugsfinder.splash;

/**
 * Created by azisa on 3/15/2017.
 */

public interface SplashView {
    void onGPSEnable();

    void onGPSDisable();
}
