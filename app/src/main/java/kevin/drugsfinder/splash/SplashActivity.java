package kevin.drugsfinder.splash;

import android.Manifest;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Build;
import android.os.Handler;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import java.security.Permission;

import kevin.drugsfinder.Config;
import kevin.drugsfinder.R;
import kevin.drugsfinder.dashboard.MainActivity;

public class SplashActivity extends AppCompatActivity implements SplashView {

    private SplashPresenter presenter;
    private String[] permissionLocation = {Manifest.permission.ACCESS_COARSE_LOCATION, Manifest.permission.ACCESS_FINE_LOCATION};
    private RelativeLayout activitySplash;
    private ProgressBar splashProgress;
    private TextView labelProgress;
    private String LocationResultSetting = "location.setting";
    private boolean result_state;
    private Handler handler;
    private Context context;
    private LocationManager locationManager;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash);

        context = this;
        handler = new Handler();
        presenter = new SplashPresenter(this, this);
        renderView();

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            if (checkSelfPermission(Manifest.permission.ACCESS_FINE_LOCATION) == PackageManager.PERMISSION_DENIED) {
                requestPermissions(permissionLocation, 200);
            } else {
                initSplashScreen();
            }

        } else {

            initSplashScreen();

        }

    }

    @Override
    protected void onResume() {
        Log.e(SplashActivity.class.getSimpleName(), "onResume: activity resume");

        super.onResume();
        if (result_state)
            presenter.checkGPSAvailable();

    }

    private void initSplashScreen() {
        presenter.checkGPSAvailable();
    }

    private void renderView() {
        activitySplash = (RelativeLayout) findViewById(R.id.activity_splash);
        splashProgress = (ProgressBar) findViewById(R.id.splash_progress);
        labelProgress = (TextView) findViewById(R.id.label_progress);
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        if (requestCode == 200) {
            if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                initSplashScreen();
            }
        }
    }

    @Override
    public void onGPSEnable() {

        locationManager = (LocationManager)
                getSystemService(Context.LOCATION_SERVICE);

        new Thread(new Runnable() {
            @Override
            public void run() {
                try {
                    Thread.sleep(5000);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }

                handler.post(new Runnable() {
                    @Override
                    public void run() {

                        LocationListener locationListener = new MyLocationListener();

                        locationManager.requestLocationUpdates(
                                LocationManager.GPS_PROVIDER, 5000, 10, locationListener);

                    }
                });

            }
        }).start();






        System.out.println("gps enabled");
    }

    @Override
    public void onGPSDisable() {
        buildAlertMessageNoGps();
    }

    private void buildAlertMessageNoGps() {
        final AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setMessage("Your GPS seems to be disabled, do you want to enable it?")
                .setCancelable(false)
                .setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                    public void onClick(@SuppressWarnings("unused") final DialogInterface dialog, @SuppressWarnings("unused") final int id) {
                        startActivityForResult(new Intent(android.provider.Settings.ACTION_LOCATION_SOURCE_SETTINGS), 111);
                        result_state = true;
                    }
                })
                .setNegativeButton("No", new DialogInterface.OnClickListener() {
                    public void onClick(final DialogInterface dialog, @SuppressWarnings("unused") final int id) {
                        dialog.cancel();
                    }
                });
        final AlertDialog alert = builder.create();
        alert.show();
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        outState.putBoolean(LocationResultSetting, result_state);
        super.onSaveInstanceState(outState);
    }

    @Override
    protected void onRestoreInstanceState(Bundle savedInstanceState) {
        super.onRestoreInstanceState(savedInstanceState);
        if (savedInstanceState != null)
            result_state = savedInstanceState.getBoolean(LocationResultSetting);
    }

    private class MyLocationListener implements LocationListener {

        @Override
        public void onLocationChanged(Location loc) {

            System.out.println("current latitude " + loc.getLatitude());

            Config.getInstance(context).setCurrentLatLng(loc.getLatitude(), loc.getLongitude());

            Intent intent = new Intent(context, MainActivity.class);
            intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
            startActivity(intent);


            locationManager.removeUpdates(this);


        }

        @Override
        public void onProviderDisabled(String provider) {}

        @Override
        public void onProviderEnabled(String provider) {}

        @Override
        public void onStatusChanged(String provider, int status, Bundle extras) {}
    }

}
