package kevin.drugsfinder.splash;

import android.content.Context;
import android.location.LocationManager;

/**
 * Created by azisa on 3/15/2017.
 */

public class SplashPresenter {
    private SplashView view;
    private Context context;

    public SplashPresenter(Context context, SplashView view) {
        this.view = view;
        this.context = context;
    }

    public void checkGPSAvailable() {
        LocationManager manager = (LocationManager)context.getSystemService(Context.LOCATION_SERVICE);
        boolean providerEnabled = manager.isProviderEnabled(LocationManager.GPS_PROVIDER);
        if(providerEnabled)
            view.onGPSEnable();
        else
            view.onGPSDisable();
    }
}
